package iteration

import "fmt"
import "testing"

func TestRepeat(t *testing.T) {
	result := Repeat("*", 6)
	expected := "******"

	if result != expected {
		t.Errorf("expected '%q' but got '%q'", expected, result)
	}
}

func ExampleRepeat() {
	result := Repeat("8", 9)
	fmt.Println(result)
	// Output: 888888888
}

func BenchmarkRepeat(b * testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 7)
	}
}